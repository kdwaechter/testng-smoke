import org.hamcrest.MatcherAssert;
import org.testng.annotations.Test;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Created by Kurt on 11/23/15.
 */
public class SmokeTestIT {

    @Test
    public void smoke(){
        int firstVal = 2;
        int secondVal = 1;
        assertThat("Values should match", firstVal == secondVal );
        System.out.println("Test Completed");
    }
}
